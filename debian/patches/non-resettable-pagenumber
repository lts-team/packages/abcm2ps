Description: Add "$Q" key in header for page numbers that don't reset
  abcm2ps supports a "$P" key in header or footer lines that will output
  the current page number and can be reset using "%%newpage". In my
  books of SCD ball sets, I like to include both the page number inside
  the tune and the page number inside the book on every page, so the
  "$Q" adds another page number that is not changeable by the user.
Author: Anselm Lingnau <lingnau@debian.org>
Last-Update: 2014-10-18

--- abcm2ps-7.8.9.orig/abc2ps.c
+++ abcm2ps-7.8.9/abc2ps.c
@@ -42,6 +42,7 @@ struct SYMBOL *sym;		/* (points to the s
 
 int tunenum;			/* number of current tune */
 int pagenum = 1;		/* current page in output file */
+int pagenum_nr = 1;		/* current page (non-resettable) */
 
 				/* switches modified by command line flags: */
 int quiet;			/* quiet mode */
--- abcm2ps-7.8.9.orig/abc2ps.h
+++ abcm2ps-7.8.9/abc2ps.h
@@ -318,6 +318,7 @@ extern int use_buffer;		/* 1 if lines ar
 extern int outft;		/* last font in the output file */
 extern int tunenum;		/* number of current tune */
 extern int pagenum;		/* current page number */
+extern int pagenum_nr;		/* current page (non-resettable) */
 extern int nbar;		/* current measure number */
 extern int in_page;
 extern int defl;		/* decoration flags */
--- abcm2ps-7.8.9.orig/buffer.c
+++ abcm2ps-7.8.9/buffer.c
@@ -404,6 +404,18 @@ static void format_hf(char *d, char *p)
 			}
 			d += sprintf(d, "%d", pagenum);
 			break;
+		case 'Q':		/* non-resetting page number */
+			if (p[1] == '0') {
+				p++;
+				if (pagenum_nr & 1)
+					break;
+			} else if (p[1] == '1') {
+				p++;
+				if ((pagenum_nr & 1) == 0)
+					break;
+			}
+			d += sprintf(d, "%d", pagenum_nr);
+			break;
 		case 'T':		/* tune title */
 			q = &info['T' - 'A']->as.text[2];
 			tex_str(q);
@@ -613,6 +625,7 @@ static void init_page(void)
 	if (cfmt.footer)
 		maxy -= headfooter(0, pwidth, pheight);
 	pagenum++;
+	pagenum_nr++;
 	outft = -1;
 }
 
